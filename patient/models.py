from django.db import models

# Create your models here.
class Patient(models.Model):
    FirstName = models.TextField('Имя', default="")
    SecondName = models.TextField('Фамилия', default="")
    Address = models.TextField('Адрес', default="")
    Sex = models.TextField('Пол', default="")
    Birthday = models.DateField('День рождения')

    def __str__(self):
        return self.FirstName

    class Meta:
        verbose_name = 'Пациент'
        verbose_name_plural = 'Пациенты'