from django.db import models

import sys

# Create your models here.
class Therapy(models.Model):
    Drug = models.TextField('Лекарство', default='')
    Action = models.TextField('Действие', default='')
    Side = models.TextField('Побочные действия', default='')

    def __str__(self):
        return self.Drug

    class Meta:
        verbose_name = 'Способ лечения'
        verbose_name_plural = 'Способы лечения'