from django.db import models

import sys

# Create your models here.
class Visit(models.Model):
    Symptomes = models.TextField('Симптом', default="")
    Diagnos = models.TextField('Диагноз', default="")
    Prescript = models.TextField('Предписание', default="")
    Patient = models.ForeignKey('patient.Patient', name='Пациент', on_delete=models.CASCADE)
    Therapy = models.ManyToManyField('therapy.Therapy')

    def __str__(self):
        return self.Diagnos

    class Meta:
        verbose_name = 'Визит'
        verbose_name_plural = 'Визиты'